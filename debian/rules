#!/usr/bin/make -f

# -lto because I use CMake's LTO support
#export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=+all,-lto qa=+all nocheck
export DEB_BUILD_MAINT_OPTIONS = nocheck

# added by qa=+bug, triggers some false positives
export DEB_CXXFLAGS_MAINT_STRIP = -Werror=array-bounds

# yuzu requires CX16 instructions (CMPXCHG16B) to build, and they are
# "only" available on Intel Core 2 or newer CPUs. The only way I know to
# express that a Debian package depends on a set of instructions not
# present in the baseline is to make it depend on an isa-support package.
# Only sse3- and sse4.2-support are available, and there are some CPUs that
# support SSE3 while not supporting CX16, so depending on that wouldn't be
# strict enough. The only safe dependency is then sse4.2-support.
# But depending on sse4.2-support while not making use of the SSE4.2
# extensions would be a waste, and it makes sense to allow GCC to generate
# optimized code that uses SSE4.2 extensions, for performance reasons.
# As both CMPXCHG16B and SSE4_2 are part of the x86-64-v2
# micro-architecture level, I can simply tell GCC to generate code for that
# target.
# Upstream is considering building with SSE4.2 anyway.
# References:
# https://github.com/yuzu-emu/yuzu/commit/626cc44d7ababf037cbf1f0a6fd1372efc296b64
# https://gitlab.com/x86-psABIs/x86-64-ABI/-/wikis/uploads/01de35b2c8adc7545de52604cc45d942/x86-64-psABI-2021-05-20.pdf#page=16
# https://github.com/yuzu-emu/yuzu/pull/7497

# -Wabi-tag -D_GLIBCXX_USE_CXX11_ABI=0 -fno-use-cxa-atexit -> custom compiler, prevents errors with __dso_handle
#export DEB_CFLAGS_MAINT_APPEND   = -march=x86-64-v2 -Wabi-tag -D_GLIBCXX_USE_CXX11_ABI=0 -fno-use-cxa-atexit
#export DEB_CXXFLAGS_MAINT_APPEND = -march=x86-64-v2 -Wabi-tag -D_GLIBCXX_USE_CXX11_ABI=0 -fno-use-cxa-atexit

export DEB_CFLAGS_MAINT_APPEND   = -march=x86-64-v2
export DEB_CXXFLAGS_MAINT_APPEND = -march=x86-64-v2

test := false

# DEB_VERSION, DEB_VENDOR
include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/vendor.mk

%:
	dh $@

override_dh_clean:
	# dh_clean: error: mv debian/.debhelper/bucket/files/05a24a2cb33bddf1c8962423b04ee824a1c2763613ac48da5a087f7f2624a9e6.tmp externals/sdl2/SDL/build-scripts/config.guess: No such file or directory

# Don't use boost system
override_dh_auto_configure:
	dh_auto_configure -- \
		-DGIT_BRANCH=$(DEB_VENDOR) \
		-DGIT_DESC=mainline-$(DEB_VERSION) \
		-DGIT_REV=mainline-$(DEB_VERSION) \
		-DCMAKE_INSTALL_BINDIR=games \
		-DCHECK_SUBMODULES_PRESENT=false \
		-DENABLE_QT_TRANSLATION=true \
		-DENABLE_FFMPEG_AUDIO_DECODER=true \
		-DENABLE_FFMPEG_AUDIO_DUMPER=true \
		-DCITRA_USE_BUNDLED_FFMPEG=false \
		-DUSE_SYSTEM_SDL2=true \
		-DCITRA_USE_BUNDLED_QT=false \
		-DUSE_SYSTEM_BOOST=false \
		-DYUZU_TESTS=$(test) \
		-DSIRIT_USE_SYSTEM_SPIRV_HEADERS=true \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=ON \
		-DCMAKE_POLICY_DEFAULT_CMP0069=NEW

# Remove tsl-robin-map related files
execute_after_dh_auto_install:
	rm -rf debian/citra/usr/share/cmake
	rm -rf debian/citra/usr/include

override_dh_dwz:
	# dwz: Too few files for multifile optimization
